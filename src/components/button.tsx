import React from 'react';
import Button from '@mui/material/Button';

interface IProps {
  className: any;
  variant: any;
  color: any;
  text: string;
  onClick: (e: any) => void | any;
}

const Btn = (props: IProps) => {
  return (
    <Button
      className={props.className}
      variant={props.variant}
      onClick={props.onClick}
      color={props.color}
    >
      {props.text}
    </Button>
  );
};

export default Btn;
