import React from 'react';
import Btn from './button';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';

interface IProps {
  save_className: any;
  save_variant: any;
  save_text: string;
  save_color: any;
  save_onClick: (e: any) => void | any;
  cancel_className: any;
  cancel_variant: any;
  cancel_text: string;
  cancel_color: any;
  cancel_onClick: (e: any) => void | any;
}

const ButtonMenu_input_update = (props: IProps) => {
  return (
    <Box display='flex'>
      <Box m='auto'>
        <Stack
          spacing={2}
          direction='row'
        >
          <Btn
            className={props.save_className}
            variant={props.save_variant}
            onClick={props.save_onClick}
            text={props.save_text}
            color={props.save_color}
          />
          <Btn
            className={props.cancel_className}
            variant={props.cancel_variant}
            onClick={props.cancel_onClick}
            text={props.cancel_text}
            color={props.cancel_color}
          />
        </Stack>
      </Box>
    </Box>
  );
};

export default ButtonMenu_input_update;
