import React from 'react';
import TextField from '@mui/material/TextField';

interface IProps {
  id: string;
  label: string;
  value: string | number;
  type: string;
  required: boolean;
  error: boolean;
  helperText: string | boolean;
  onChange: (e: any) => void | any;
}

const TextInput_date = (props: IProps) => {
  const dayToday = new Date().toISOString().slice(0, 10);
  return (
    <TextField
      id={props.id}
      variant='filled'
      size='small'
      label={props.label}
      value={props.value}
      onChange={props.onChange}
      type={props.type}
      required={props.required}
      error={props.error}
      helperText={props.helperText}
      InputProps={{
        inputProps: { min: '1900-01-01', max: dayToday },
      }}
      InputLabelProps={{ shrink: true }}
    />
  );
};

export default TextInput_date;
