import React from 'react';
import Btn from './button';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';

interface IProps {
  submit_className: any;
  submit_variant: any;
  submit_text: string;
  submit_color: any;
  submit_onClick: (e: any) => void | any;
  reset_className: any;
  reset_variant: any;
  reset_text: string;
  reset_color: any;
  reset_onClick: (e: any) => void | any;
}

const ButtonMenu_input = (props: IProps) => {
  return (
    <Box display='flex'>
      <Box m='auto'>
        <Stack
          spacing={2}
          direction='row'
        >
          <Btn
            className={props.submit_className}
            variant={props.submit_variant}
            onClick={props.submit_onClick}
            text={props.submit_text}
            color={props.submit_color}
          />
          <Btn
            className={props.reset_className}
            variant={props.reset_variant}
            onClick={props.reset_onClick}
            text={props.reset_text}
            color={props.reset_color}
          />
        </Stack>
      </Box>
    </Box>
  );
};

export default ButtonMenu_input;
