import React from 'react';
import Btn from './button';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';

interface IProps {
  update_className: any;
  update_variant: any;
  update_text: string;
  update_color: any;
  update_onClick: (e: any) => void | any;
  delete_className: any;
  delete_variant: any;
  delete_text: string;
  delete_color: any;
  delete_onClick: (e: any) => void | any;
}

const ButtonMenu_output = (props: IProps) => {
  return (
    <Box display='flex'>
      <Box m='auto'>
        <Stack
          spacing={2}
          direction='row'
        >
          <Btn
            className={props.update_className}
            variant={props.update_variant}
            onClick={props.update_onClick}
            text={props.update_text}
            color={props.update_color}
          />
          <Btn
            className={props.delete_className}
            variant={props.delete_variant}
            onClick={props.delete_onClick}
            text={props.delete_text}
            color={props.delete_color}
          />
        </Stack>
      </Box>
    </Box>
  );
};

export default ButtonMenu_output;
