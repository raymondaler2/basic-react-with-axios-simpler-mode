import React from 'react';
import TextField from '@mui/material/TextField';

interface IProps {
  id: string;
  label: string;
  value: any;
  type: string;
  required: boolean;
  error: boolean;
  helperText: string | boolean;
  onChange: (e: any) => void | any;
}

const TextInput = (props: IProps) => {
  return (
    <TextField
      id={props.id}
      variant='filled'
      size='small'
      label={props.label}
      value={props.value}
      onChange={props.onChange}
      type={props.type}
      required={props.required}
      error={props.error}
      helperText={props.helperText}
    />
  );
};

export default TextInput;
