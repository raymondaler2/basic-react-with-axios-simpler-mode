import React from "react";
import TextInput from "./textfield";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import TextInput_date from "./textfield_date";

interface IProps {
  fname_id: string;
  fname_label: string;
  fname_type: string;
  fname_required: boolean;
  lname_id: string;
  lname_label: string;
  lname_type: string;
  lname_required: boolean;
  suffix_id: string;
  suffix_label: string;
  suffix_type: string;
  suffix_required: boolean;
  age_id: string;
  age_label: string;
  age_type: string;
  age_required: boolean;
  address_id: string;
  address_label: string;
  address_type: string;
  address_required: boolean;
  birthdate_id: string;
  birthdate_label: string;
  birthdate_type: string;
  birthdate_required: boolean;
  onChange: (e: any) => void | any;
  error: {
    last_name: boolean;
    first_name: boolean;
    suffix: boolean;
    age: boolean;
    address: boolean;
    birthdate: boolean;
  };
  value: {
    last_name: string;
    first_name: string;
    suffix: string;
    age: string;
    address: string;
    birthdate: string;
  };
}

const Form = (props: IProps) => {
  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <TextInput
          id={props.fname_id}
          label={props.fname_label}
          value={props.value.first_name}
          onChange={props.onChange}
          type={props.fname_type}
          required={props.fname_required}
          error={props.error.first_name}
          helperText={
            props.error.first_name && "Only Small and Capital Letters Allowed"
          }
        />
      </Grid>
      <Grid item xs={12}>
        <TextInput
          id={props.lname_id}
          label={props.lname_label}
          value={props.value.last_name}
          onChange={props.onChange}
          type={props.lname_type}
          required={props.lname_required}
          error={props.error.last_name}
          helperText={
            props.error.last_name && "Only Small and Capital Letters Allowed"
          }
        />
      </Grid>
      <Grid item xs={12}>
        <TextInput
          id={props.suffix_id}
          label={props.suffix_label}
          value={props.value.suffix}
          onChange={props.onChange}
          type={props.suffix_type}
          required={props.suffix_required}
          error={props.error.suffix}
          helperText={
            props.error.suffix &&
            "Only .(period) Small and Capital Letters Allowed"
          }
        />
      </Grid>
      <Grid item xs={12}>
        <TextInput
          id={props.age_id}
          label={props.age_label}
          value={props.value.age}
          onChange={props.onChange}
          type={props.age_type}
          required={props.age_required}
          error={props.error.age}
          helperText={
            props.error.age && "Age must be grater than 0 or less than 150"
          }
        />
      </Grid>
      <Grid item xs={12}>
        <TextInput
          id={props.address_id}
          label={props.address_label}
          value={props.value.address}
          onChange={props.onChange}
          type={props.address_type}
          required={props.address_required}
          error={props.error.address}
          helperText={
            props.error.address &&
            "Only .(period) ,(comma) Small and Capital Letters Allowed"
          }
        />
      </Grid>
      <Grid item xs={12}>
        <TextInput_date
          id={props.birthdate_id}
          label={props.birthdate_label}
          value={props.value.birthdate}
          onChange={props.onChange}
          type={props.birthdate_type}
          required={props.birthdate_required}
          error={props.error.birthdate}
          helperText={props.error.birthdate && "Birthdate input is not valid"}
        />
      </Grid>
    </Grid>
  );
};

export default Form;
