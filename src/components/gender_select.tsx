import React from 'react';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import Grid from '@mui/material/Grid';

interface IProps {
  error: boolean | any;
  checked: {
    radioMale: boolean;
    radioFemale: boolean;
    radioOther: boolean;
  };
  onChange: (e: any) => void | any;
}

const GenderSelect = (props: IProps) => {
  return (
    <Grid
      container
      spacing={2}
    >
      <Grid
        item
        xs={12}
      >
        <FormControl error={props.error}>
          <FormLabel>Gender*</FormLabel>
          <RadioGroup row>
            <FormControlLabel
              checked={props.checked.radioMale}
              name='gender'
              className='gender-label-color'
              id='gender'
              value='Male'
              onChange={props.onChange}
              control={<Radio />}
              label='Male'
            />
            <FormControlLabel
              checked={props.checked.radioFemale}
              name='gender'
              className='gender-label-color'
              id='gender'
              value='Female'
              onChange={props.onChange}
              control={<Radio />}
              label='Female'
            />
            <FormControlLabel
              checked={props.checked.radioOther}
              name='gender'
              className='gender-label-color'
              id='gender'
              value='Other'
              onChange={props.onChange}
              control={<Radio />}
              label='Other'
            />
          </RadioGroup>
        </FormControl>
      </Grid>
    </Grid>
  );
};

export default GenderSelect;
