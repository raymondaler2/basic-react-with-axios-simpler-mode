import * as React from 'react';
import ButtonMenu_output from './button_menu_output';
import ButtonMenu_output_update from './button_menu_output_update';
import Table from 'react-bootstrap/Table';
import TextInput from './textfield';

interface IProps {
  editData: Idata;
  output_id: string;
  data: Idata[];
  handleUpdate: (
    id: string,
    fname: string,
    lname: string,
    suffix: string,
    age: string,
    address: string,
    gender: string,
    birthdate: string,
    id_editData: string
  ) => void | any;
  handleCancel: (e: any) => void | any;
  handleDelete: (e: any) => void | any;
  handleSave: (e: any) => void | any;
  handleInputUpdate: (e: any) => void | any;
}

interface Idata {
  last_name: string;
  first_name: string;
  suffix: string;
  age: string;
  address: string;
  gender: string;
  birthdate: string;
  id?: string;
}

const TableOut = (props: IProps) => {
  let Update_delete = (data: any) => {
    return (
      <ButtonMenu_output
        update_className='button-spacing-reset'
        update_variant='contained'
        update_text='update'
        update_color='success'
        update_onClick={() =>
          props.handleUpdate(
            data.id,
            data.first_name,
            data.last_name,
            data.suffix,
            data.age,
            data.address,
            data.gender,
            data.birthdate,
            data.id
          )
        }
        delete_className='button-spacing-reset'
        delete_variant='contained'
        delete_text='delete'
        delete_color='error'
        delete_onClick={() => props.handleDelete(data.id)}
      />
    );
  };

  let Save_cancel = (data: any) => {
    return (
      <ButtonMenu_output_update
        save_className='button-spacing-reset'
        save_variant='contained'
        save_text='save'
        save_color='info'
        save_onClick={() => props.handleSave(data.id)}
        cancel_className='button-spacing-reset'
        cancel_variant='contained'
        cancel_text='cancel'
        cancel_color='secondary'
        cancel_onClick={props.handleCancel}
      />
    );
  };

  return (
    <Table
      striped
      bordered
      hover
      className='data_color'
    >
      <thead>
        <tr>
          <th>ID</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Suffix</th>
          <th>Age</th>
          <th>Address</th>
          <th>Birthdate</th>
          <th>Gender</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {props.data.map((data) => {
          if (data.first_name === '') {
          } else {
            return (
              <tr
                key={data.id}
                className='width_update'
              >
                <td>{data.id}</td>
                <td>
                  {data.id === props.output_id ? (
                    <TextInput
                      id='first_name'
                      label=''
                      value={props.editData.first_name}
                      onChange={props.handleInputUpdate}
                      type='text'
                      required={false}
                      error={false}
                      helperText={false}
                    />
                  ) : (
                    data.first_name
                  )}
                </td>
                <td>
                  {data.id === props.output_id ? (
                    <TextInput
                      id='last_name'
                      label=''
                      value={props.editData.last_name}
                      onChange={props.handleInputUpdate}
                      type='text'
                      required={false}
                      error={false}
                      helperText={false}
                    />
                  ) : (
                    data.last_name
                  )}
                </td>
                <td>
                  {data.id === props.output_id ? (
                    <TextInput
                      id='suffix'
                      label=''
                      value={props.editData.suffix}
                      onChange={props.handleInputUpdate}
                      type='text'
                      required={false}
                      error={false}
                      helperText={false}
                    />
                  ) : (
                    data.suffix
                  )}
                </td>
                <td>
                  {data.id === props.output_id ? (
                    <TextInput
                      id='age'
                      label=''
                      value={props.editData.age}
                      onChange={props.handleInputUpdate}
                      type='text'
                      required={false}
                      error={false}
                      helperText={false}
                    />
                  ) : (
                    data.age
                  )}
                </td>
                <td>
                  {data.id === props.output_id ? (
                    <TextInput
                      id='address'
                      label=''
                      value={props.editData.address}
                      onChange={props.handleInputUpdate}
                      type='text'
                      required={false}
                      error={false}
                      helperText={false}
                    />
                  ) : (
                    data.address
                  )}
                </td>
                <td>
                  {data.id === props.output_id ? (
                    <TextInput
                      id='birthdate'
                      label=''
                      value={props.editData.birthdate}
                      onChange={props.handleInputUpdate}
                      type='text'
                      required={false}
                      error={false}
                      helperText={false}
                    />
                  ) : (
                    data.birthdate
                  )}
                </td>
                <td>
                  {data.id === props.output_id ? (
                    <TextInput
                      id='gender'
                      label=''
                      value={props.editData.gender}
                      onChange={props.handleInputUpdate}
                      type='text'
                      required={false}
                      error={false}
                      helperText={false}
                    />
                  ) : (
                    data.gender
                  )}
                </td>
                <td>
                  {data.id === props.output_id
                    ? Save_cancel(data)
                    : Update_delete(data)}
                </td>
              </tr>
            );
          }
        })}
      </tbody>
    </Table>
  );
};

export default TableOut;
