import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import Button from '@mui/material/Button';
// @ts-ignore
import Table from 'react-bootstrap/Table';
import axios from 'axios';
import ButtonMenu_output from './components/button_menu_output';
import ButtonMenu_output_update from './components/button_menu_output_update';
import TextInput from './components/textfield';

interface Istate {
  input: data;
  edit: data;
  output: data[];
}

interface radio_in {
  radioMale: boolean;
  radioFemale: boolean;
  radioOther: boolean;
}
interface data {
  id?: string;
  fname: string;
  lname: string;
  suffix: string;
  age: string;
  address: string;
  bdate: string;
  gender: string;
}

interface error {
  last_name: boolean;
  first_name: boolean;
  suffix: boolean;
  age: boolean;
  address: boolean;
  birthdate: boolean;
  gender?: boolean;
}

function App() {
  const [state, setState] = useState<Istate>({
    input: {
      id: '',
      fname: '',
      lname: '',
      suffix: '',
      age: '',
      address: '',
      bdate: '',
      gender: '',
    },
    edit: {
      id: '',
      fname: '',
      lname: '',
      suffix: '',
      age: '',
      address: '',
      bdate: '',
      gender: '',
    },
    output: [],
  });

  const [radio, setRadio] = useState({
    radioMale: false,
    radioFemale: false,
    radioOther: false,
  });

  const [error, setError] = useState<error>({
    last_name: false,
    first_name: false,
    suffix: false,
    age: false,
    address: false,
    birthdate: false,
    gender: false,
  });

  // ! Generates an error array.
  const generateErrorArray = (
    first_name: string,
    last_name: string,
    suffix: string,
    age: string,
    address: string,
    birthdate: string,
    gender: string
  ) => {
    let ErrorArray: boolean[] = [];
    let emptyInputCheckArray = validateIfEmpty([
      first_name,
      last_name,
      suffix,
      age,
      address,
      birthdate,
      gender,
    ]);
    let dataInputCheckArray = validateDataInput(
      first_name,
      last_name,
      suffix,
      age,
      address
    );

    for (let x = 0; x < 7; x++) {
      if (emptyInputCheckArray[x] || !dataInputCheckArray[x]) {
        ErrorArray.push(true);
      } else {
        ErrorArray.push(false);
      }
    }
    return ErrorArray;
  };

  // !Returns an array of booleans indicating if the data is empty or not
  const validateIfEmpty = (arr: any[]) => {
    const validationArray = arr.map((data) => {
      if (data === '') return true;
      else return false;
    });

    // * since last name and suffix is not required, we return false
    if (validationArray[1] === true) validationArray[1] = false;
    if (validationArray[2] === true) validationArray[2] = false;
    return validationArray;
  };

  // ! Returns an array of booleans indicating if the data is valid
  const validateDataInput = (
    first_name: string,
    last_name: string,
    suffix: string,
    age: string,
    address: string
  ) => {
    const arr = [];

    if (first_name.match(/^[A-Za-z ]+$/)) {
      arr.push(true);
    } else if (first_name === '') {
      arr.push(false);
    } else {
      arr.push(false);
    }
    if (last_name.match(/^[A-Za-z ]+$/)) {
      arr.push(true);
    } else if (last_name === '') {
      arr.push(true);
    } else {
      arr.push(false);
    }
    if (suffix.match(/^[A-Za-z. ]+$/)) {
      arr.push(true);
    } else if (suffix === '') {
      arr.push(true);
    } else {
      arr.push(false);
    }
    if (parseInt(age) < 0) {
      arr.push(false);
    } else if (parseInt(age) <= 150) {
      arr.push(true);
    } else if (parseInt(age) >= 150) {
      arr.push(false);
    } else {
      arr.push(true);
    }
    if (address.match(/^[A-Za-z0-9,. ]+$/)) {
      arr.push(true);
    } else if (address === '') {
      arr.push(false);
    } else {
      arr.push(false);
    }
    if (parseInt(age) < 0) {
      arr.push(false);
    } else if (parseInt(age) <= 150) {
      // * for birthdate boolean value
      arr.push(true);
    } else if (parseInt(age) >= 150) {
      arr.push(false);
    } else {
      arr.push(true);
    }
    arr.push(true); // * for gender boolean value

    return arr;
  };

  const SubmitHandler = async () => {
    const { fname, lname, suffix, age, address, bdate, gender } = state.input;
    const falseArr = [false, false, false, false, false, false, false];
    const ErrorArray = generateErrorArray(
      fname,
      lname,
      suffix,
      age,
      address,
      bdate,
      gender
    );
    const empty = {
      id: '',
      fname: '',
      lname: '',
      suffix: '',
      age: '',
      address: '',
      bdate: '',
      gender: '',
    };

    if (JSON.stringify(ErrorArray) === JSON.stringify(falseArr)) {
      const result = await axios
        .post('/Data', state.input)
        .then((response) => response);
      if (result.data.code === 200) {
        setState((previous) => ({
          ...previous,
          input: empty,
        }));
        fetchDataHandler();
      }
      setRadio({
        radioMale: false,
        radioFemale: false,
        radioOther: false,
      });
    } else {
      setError({
        first_name: ErrorArray[0],
        last_name: ErrorArray[1],
        suffix: ErrorArray[2],
        age: ErrorArray[3],
        address: ErrorArray[4],
        birthdate: ErrorArray[5],
        gender: ErrorArray[6],
      });
    }
  };

  const fetchDataHandler = async () => {
    const result = await axios.get('/Data').then((response) => response);
    setState((previous) => ({
      ...previous,
      output: result.data,
    }));
  };

  const InputHandler = (event: any) => {
    const resetErrorToFalse = {
      first_name: false,
      last_name: false,
      age: false,
      address: false,
      birthdate: false,
      gender: false,
      suffix: false,
    };
    let { value, id, name } = event.target;
    let idString = Math.floor(Math.random() * (1000 - 100 + 1)) + 100;

    if (name === 'gender') {
      id = name;
      switch (value) {
        case 'Male': {
          setRadio({
            radioMale: true,
            radioFemale: false,
            radioOther: false,
          });
          setState((previous) => ({
            ...previous,
            input: {
              ...state.input,
              [id]: value,
              gender: value,
            },
          }));
          setError(resetErrorToFalse);
          break;
        }
        case 'Female': {
          setRadio({
            radioMale: false,
            radioFemale: true,
            radioOther: false,
          });
          setState((previous) => ({
            ...previous,
            input: {
              ...state.input,
              [id]: value,
              gender: value,
            },
          }));
          setError(resetErrorToFalse);
          break;
        }
        case 'Other': {
          setRadio({
            radioMale: false,
            radioFemale: false,
            radioOther: true,
          });
          setState((previous) => ({
            ...previous,
            input: {
              ...state.input,
              [id]: value,
              gender: value,
              id: idString.toString(),
            },
          }));
          setError(resetErrorToFalse);
          break;
        }
      }
    }

    if (value.indexOf('  ') >= 0) {
      // * check if value is more than 1 space then trim
      value.trim();
    } else {
      if (/^\s/.test(value)) {
        // * check white space at the start of value then set to blank
        value = '';
      }
      if (id === 'bdate') {
        let dayToday = new Date().toISOString().slice(0, 10);
        let ageCalculation =
          parseInt(dayToday.split('-')[0]) - parseInt(value.split('-')[0]);
        setState((previous) => ({
          ...previous,
          input: {
            ...state.input,
            [id]: value,
            age: ageCalculation.toString(),
          },
        }));
        setError(resetErrorToFalse);
      } else if (id === 'fname') {
        setState((previous) => ({
          ...previous,
          input: {
            ...state.input,
            id: idString.toString(),
            [id]: value,
          },
        }));
        setError(resetErrorToFalse);
      } else {
        setState((previous) => ({
          ...previous,
          input: {
            ...state.input,
            [id]: value,
          },
        }));
        setError(resetErrorToFalse);
      }
    }
  };

  const UpdateHandler = (data: data) => {
    setState((previous) => ({
      ...previous,
      edit: data,
    }));
  };

  const CancelHandler = () => {
    setState((previous) => ({
      ...previous,
      edit: {
        id: '',
        fname: '',
        lname: '',
        suffix: '',
        age: '',
        address: '',
        bdate: '',
        gender: '',
      },
    }));
  };

  const DeleteHandler = async (id?: string) => {
    const result = await axios
      .delete(`/Data/${id}`)
      .then((response) => response);
    if (result.data.code === 200) fetchDataHandler();
  };

  const SaveHandler = async () => {
    const result = await axios
      .put(`/Data/`, state.edit)
      .then((response) => response);
    setState((previous) => ({
      ...previous,
      edit: {
        id: '',
        fname: '',
        lname: '',
        suffix: '',
        age: '',
        address: '',
        bdate: '',
        gender: '',
      },
    }));
    fetchDataHandler();
  };

  const InputUpdateHandler = (event: any) => {
    const { id, value } = event.target;
    setState((previous) => ({
      ...previous,
      edit: {
        ...state.edit,
        [id]: value,
      },
    }));
  };

  useEffect(() => {
    fetchDataHandler();
  }, []);

  return (
    <div className='App'>
      <header className='App-header'>
        <Container sx={{ width: '40%' }}>
          <Typography
            variant='h2'
            gutterBottom
          >
            Input Form
          </Typography>
          <Grid
            container
            spacing={2}
            className='white'
          >
            <Grid
              item
              alignItems='center'
              justifyContent='center'
              xs={12}
            >
              <TextField
                fullWidth
                id='fname'
                label='First Name'
                onChange={InputHandler}
                value={state.input.fname}
                error={error.first_name}
                helperText={
                  error.first_name && 'Only Small and Capital Letters Allowed'
                }
              />
            </Grid>
            <Grid
              item
              alignItems='center'
              justifyContent='center'
              xs={12}
            >
              <TextField
                fullWidth
                id='lname'
                label='Last Name'
                onChange={InputHandler}
                value={state.input.lname}
                error={error.last_name}
                helperText={
                  error.last_name && 'Only Small and Capital Letters Allowed'
                }
              />
            </Grid>
            <Grid
              item
              alignItems='center'
              justifyContent='center'
              xs={12}
            >
              <TextField
                fullWidth
                id='suffix'
                label='Suffix'
                onChange={InputHandler}
                value={state.input.suffix}
                error={error.suffix}
                helperText={
                  error.suffix &&
                  'Only .(period) Small and Capital Letters Allowed'
                }
              />
            </Grid>
            <Grid
              item
              alignItems='center'
              justifyContent='center'
              xs={12}
            >
              <TextField
                fullWidth
                type='number'
                id='age'
                label='Age'
                onChange={InputHandler}
                value={state.input.age}
                error={error.age}
                helperText={
                  error.age && 'Age must be grater than 0 or less than 150'
                }
              />
            </Grid>
            <Grid
              item
              alignItems='center'
              justifyContent='center'
              xs={12}
            >
              <TextField
                fullWidth
                id='address'
                label='Address'
                onChange={InputHandler}
                value={state.input.address}
                error={error.address}
                helperText={
                  error.address &&
                  'Only .(period) ,(comma) Small and Capital Letters Allowed'
                }
              />
            </Grid>
            <Grid
              item
              alignItems='center'
              justifyContent='center'
              xs={12}
            >
              <TextField
                fullWidth
                type='date'
                InputLabelProps={{ shrink: true }}
                InputProps={{
                  inputProps: {
                    min: '1900-01-01',
                    max: new Date().toISOString().slice(0, 10),
                  },
                }}
                id='bdate'
                label='Birthdate'
                onChange={InputHandler}
                value={state.input.bdate}
                error={error.birthdate}
                helperText={error.birthdate && 'Birthdate input is not valid'}
              />
            </Grid>
            <Grid
              item
              alignItems='center'
              justifyContent='center'
              xs={12}
            >
              <Grid
                container
                spacing={2}
              >
                <Grid
                  item
                  xs={12}
                >
                  <FormControl error={error.gender}>
                    <FormLabel>Gender*</FormLabel>
                    <RadioGroup row>
                      <FormControlLabel
                        onChange={InputHandler}
                        className='gender-label-color'
                        checked={radio.radioMale}
                        name='gender'
                        id='gender'
                        value='Male'
                        control={<Radio />}
                        label='Male'
                      />
                      <FormControlLabel
                        onChange={InputHandler}
                        className='gender-label-color'
                        checked={radio.radioFemale}
                        name='gender'
                        id='gender'
                        value='Female'
                        control={<Radio />}
                        label='Female'
                      />
                      <FormControlLabel
                        onChange={InputHandler}
                        className='gender-label-color'
                        checked={radio.radioOther}
                        name='gender'
                        id='gender'
                        value='Other'
                        control={<Radio />}
                        label='Other'
                      />
                    </RadioGroup>
                  </FormControl>
                </Grid>
                <Grid
                  item
                  xs={12}
                >
                  <Button
                    variant='contained'
                    color='primary'
                    onClick={SubmitHandler}
                  >
                    Submit
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Container>
        <br />
        <br />
        <Container sx={{ width: '90%' }}>
          <Typography
            variant='h2'
            gutterBottom
          >
            Output Table
          </Typography>
          <Grid
            container
            spacing={2}
            className='white'
          >
            <Grid
              item
              alignItems='center'
              justifyContent='center'
              xs={12}
            >
              <Table
                striped
                bordered
                hover
                className='data_color'
              >
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Suffix</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th>Birthdate</th>
                    <th>Gender</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {state.output.map((data) => {
                    return state.edit.id !== data.id ? (
                      <tr>
                        <td>{data.id}</td>
                        <td>{data.fname}</td>
                        <td>{data.lname}</td>
                        <td>{data.suffix}</td>
                        <td>{data.age}</td>
                        <td>{data.address}</td>
                        <td>{data.bdate}</td>
                        <td>{data.gender}</td>
                        <td>
                          <ButtonMenu_output
                            update_className='button-spacing-reset'
                            update_variant='contained'
                            update_text='update'
                            update_color='success'
                            update_onClick={() => UpdateHandler(data)}
                            delete_className='button-spacing-reset'
                            delete_variant='contained'
                            delete_text='delete'
                            delete_color='error'
                            delete_onClick={() => DeleteHandler(data.id)}
                          />
                        </td>
                      </tr>
                    ) : (
                      <tr>
                        <td>{data.id}</td>
                        <td>
                          <TextInput
                            id='fname'
                            label=''
                            value={state.edit.fname}
                            onChange={InputUpdateHandler}
                            type='text'
                            required={false}
                            error={false}
                            helperText={false}
                          />
                        </td>
                        <td>
                          <TextInput
                            id='lname'
                            label=''
                            value={state.edit.lname}
                            onChange={InputUpdateHandler}
                            type='text'
                            required={false}
                            error={false}
                            helperText={false}
                          />
                        </td>
                        <td>
                          <TextInput
                            id='suffix'
                            label=''
                            value={state.edit.suffix}
                            onChange={InputUpdateHandler}
                            type='text'
                            required={false}
                            error={false}
                            helperText={false}
                          />
                        </td>
                        <td>
                          <TextInput
                            id='age'
                            label=''
                            value={state.edit.age}
                            onChange={InputUpdateHandler}
                            type='number'
                            required={false}
                            error={false}
                            helperText={false}
                          />
                        </td>
                        <td>
                          <TextInput
                            id='address'
                            label=''
                            value={state.edit.address}
                            onChange={InputUpdateHandler}
                            type='text'
                            required={false}
                            error={false}
                            helperText={false}
                          />
                        </td>
                        <td>
                          <TextInput
                            id='bdate'
                            label=''
                            value={state.edit.bdate}
                            onChange={InputUpdateHandler}
                            type='date'
                            required={false}
                            error={false}
                            helperText={false}
                          />
                        </td>
                        <td>
                          <TextInput
                            id='gender'
                            label=''
                            value={state.edit.gender}
                            onChange={InputUpdateHandler}
                            type='text'
                            required={false}
                            error={false}
                            helperText={false}
                          />
                        </td>
                        <td>
                          <ButtonMenu_output_update
                            save_className='button-spacing-reset'
                            save_variant='contained'
                            save_text='save'
                            save_color='info'
                            save_onClick={SaveHandler}
                            cancel_className='button-spacing-reset'
                            cancel_variant='contained'
                            cancel_text='cancel'
                            cancel_color='secondary'
                            cancel_onClick={CancelHandler}
                          />
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </Grid>
          </Grid>
        </Container>
      </header>
    </div>
  );
}

export default App;
